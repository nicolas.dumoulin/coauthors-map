# -*- coding: utf-8 -*-
import csv, os, sys
import bibtexparser
from nominatim import Nominatim

bibkeys = ['author', 'title', 'year', 'journal', 'link']
bibInputFile = 'lisc.bib'
citiesCoAuthorsFile = 'Export010218-FJ-LISC-vf-1.csv'

# Loading BibTex references
if not os.path.isfile(bibInputFile):
    print(bibInputFile + ' file not found. Please provide a BibTeX file of the publications.')
    sys.exit(1)
with open(bibInputFile) as bibtex_file:
    bib_database = bibtexparser.loads(bibtex_file.read())

# Loading Cities geolocation
nom = Nominatim()
cities={}
if not os.path.isfile('cities.csv'):
    with open('cities.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(['City','Lat','Lon'])
else:
    with open('cities.csv') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        cities = {row[0]:row[1:] for row in reader}

# Reading list of publication cities extracted from irsteadoc by Sybille De Maréchal (Irstea)
records=[]
if not os.path.isfile(citiesCoAuthorsFile):
    print(citiesCoAuthorsFile + ' file not found. Please provide a file listing'
        + ' the publications with the list of cities of coauthors.\n'
        + ' CSV format expected with columns: IrsteaDoc ID,Year,Title,City1,City2,…')
    sys.exit(1)
with open(citiesCoAuthorsFile) as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    next(reader)
    for row in reader:
        pub = row[0].strip()
        entry = bib_database.entries_dict.get(pub, None)
        if not entry:
            print('no bibtex entry for {}'.format(row))
        for city in row[3:]:
            city=city.strip()
            if len(city)>0:
                if city in cities.keys():
                    # city already known
                    lat,lon=cities[city]
                else:
                    # requesting the position of the city
                    result = nom.query(city)
                    if len(result) == 0:
                        print('Geocoding failed for {}'.format(city))
                        lat=0
                        lon=0
                    else:
                        # save the found position of the city
                        lat=result[0]['lat']
                        lon=result[0]['lon']
                        cities[city] = [lat,lon]
                        with open('cities.csv', 'a') as citiesfile:
                            writer = csv.writer(citiesfile, delimiter=',')
                            writer.writerow([city,lat,lon])
                record = {
                    'City':city.strip(),'lat':lat,'lon':lon,
                    #'PUB':pub,'Year':row[1].strip(),'Title':row[2].strip(),
                    }
                for k in bibkeys:
                    if k in entry:
                        record[k] = entry[k]
                #if entry:
                #    record['bib'] = entry
                records.append(record)
# Save the result in a CSV file
with open('publisByCities.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    outputkeys = ['City', 'lon', 'lat'] + bibkeys
    writer.writerow(outputkeys)
    for record in records:
        writer.writerow([record.get(k,'') for k in outputkeys])
